"""Editable Behaviour ...
License: LGPL
"""
__all__ = ('EditBehavior',)
__author__ = 'Roberto Biedma'

from enum import Enum

from kivy.properties import BooleanProperty, NumericProperty, ListProperty
from kivy.uix.behaviors import DragBehavior
from kivy.uix.widget import Widget

edit_behavior_msg_counter = 0


def get_msg_count():
    global edit_behavior_msg_counter
    edit_behavior_msg_counter += 1
    return edit_behavior_msg_counter


class HandlerType(Enum):
    """Indicates the different types of edit handlers available"""
    LEFT = 'left'
    TOP = 'top'
    RIGHT = 'right'
    BOTTOM = 'bottom'
    LEFT_TOP = 'left_top'
    RIGHT_TOP = 'right_top'
    RIGHT_BOTTOM = 'right_bottom'
    LEFT_BOTTOM = 'left_bottom'


class CornerEditHandler(DragBehavior, Widget):
    """A widget that when dragged allows modification of position and size of the parent in two dimensions"""
    handler_type = None
    side_length = NumericProperty(10)
    is_pos_to_parent_bind = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.handler_type = kwargs.pop("handler_type")
        super(CornerEditHandler, self).__init__(**kwargs)

    def update_pos(self, touch, observable):
        if self.parent:
            if self.handler_type == HandlerType.LEFT_TOP:
                self.pos = self.parent.x - self.size[0], self.parent.top
            elif self.handler_type == HandlerType.RIGHT_TOP:
                self.pos = self.parent.right, self.parent.top
            elif self.handler_type == HandlerType.RIGHT_BOTTOM:
                self.pos = self.parent.right, self.parent.y - self.size[1]
            elif self.handler_type == HandlerType.LEFT_BOTTOM:
                self.pos = self.parent.x - self.size[0], self.parent.y - self.size[1]

    def bind_pos_to_parent(self):
        if not self.is_pos_to_parent_bind:
            self.bind(pos=self._update_parent_pos_size)
            self.is_pos_to_parent_bind = True

    def unbind_pos_to_parent(self):
        if self.is_pos_to_parent_bind:
            self.unbind(pos=self._update_parent_pos_size)
            self.is_pos_to_parent_bind = False

    def _update_parent_pos_size(self, touch, observable):
        if self.parent:
            if self.handler_type == HandlerType.LEFT_TOP:
                self.parent.pos[0] = self.x + self.size[0]
                self.parent.size = self.parent.current_size[0] + self.parent.current_pos[
                    0] - self.parent.x, self.y - self.parent.y
            elif self.handler_type == HandlerType.RIGHT_TOP:
                self.parent.size = self.x - self.parent.x, self.y - self.parent.y
            elif self.handler_type == HandlerType.RIGHT_BOTTOM:
                self.parent.pos[1] = self.y + self.size[1]
                self.parent.size = self.x - self.parent.x, self.parent.current_size[1] + self.parent.current_pos[
                    1] - self.parent.y
            elif self.handler_type == HandlerType.LEFT_BOTTOM:
                self.parent.pos = self.x + self.size[0], self.y + self.size[1]
                self.parent.size = self.parent.current_size[0] + self.parent.current_pos[0] - self.parent.x, \
                                   self.parent.current_size[1] + self.parent.current_pos[1] - self.parent.y


class EditHandler(DragBehavior, Widget):
    """A widget that when dragged allows modification of position and size of the parent in one dimension"""
    handler_type = None
    side_length = NumericProperty(10)
    is_pos_to_parent_bind = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.handler_type = kwargs.pop("handler_type")
        super(EditHandler, self).__init__(**kwargs)

    def update_pos(self, touch, observable):
        if self.parent:
            if self.handler_type == HandlerType.LEFT:
                self.pos = self.parent.x - self.size[0], self.parent.center_y - self.size[1] / 2
            elif self.handler_type == HandlerType.TOP:
                self.pos = self.parent.center_x - self.size[0] / 2, self.parent.y + self.parent.size[1]
            elif self.handler_type == HandlerType.RIGHT:
                self.pos = self.parent.x + self.parent.size[0], self.parent.center_y - self.size[1] / 2
            elif self.handler_type == HandlerType.BOTTOM:
                self.pos = [self.parent.center_x - self.size[0] / 2, self.parent.y - self.size[1]]

    def bind_pos_to_parent(self):
        if not self.is_pos_to_parent_bind:
            self.bind(pos=self._update_parent_pos_size)
            self.is_pos_to_parent_bind = True

    def unbind_pos_to_parent(self):
        if self.is_pos_to_parent_bind:
            self.unbind(pos=self._update_parent_pos_size)
            self.is_pos_to_parent_bind = False

    def _update_parent_pos_size(self, touch, observable):
        if self.parent:
            if self.handler_type == HandlerType.LEFT:
                self.parent.pos[0] = self.pos[0] + self.size[0]
                self.parent.size[0] = self.parent.current_size[0] + self.parent.current_pos[0] - self.parent.pos[0]
                self.pos[1] = self.center_y - self.size[1] / 2
            elif self.handler_type == HandlerType.TOP:
                self.parent.size[1] = self.pos[1] - self.parent.pos[1]
                self.pos[0] = self.parent.center_x - self.size[0] / 2
            elif self.handler_type == HandlerType.RIGHT:
                self.parent.size[0] = self.pos[0] - self.parent.pos[0]
                self.pos[1] = self.parent.center_y - self.size[1] / 2
            elif self.handler_type == HandlerType.BOTTOM:
                self.parent.pos[1] = self.pos[1] + self.size[1]
                self.parent.size[1] = self.parent.current_size[1] + self.parent.current_pos[1] - self.parent.pos[1]
                self.pos[0] = self.parent.center_x - self.size[0] / 2


class EditBehavior(object):
    selected = BooleanProperty(False)
    side_handlers = [HandlerType.LEFT, HandlerType.TOP, HandlerType.RIGHT, HandlerType.BOTTOM]
    corner_handlers = [HandlerType.LEFT_TOP, HandlerType.RIGHT_TOP, HandlerType.RIGHT_BOTTOM, HandlerType.LEFT_BOTTOM]
    handler_types = []
    handler_types.extend(side_handlers)
    handler_types.extend(corner_handlers)
    handlers = ListProperty()
    current_pos = 0, 0
    current_size = 0, 0

    def __init__(self, **kwargs):
        super(EditBehavior, self).__init__(**kwargs)

    def _do_selected(self):
        self.selected = True
        if self.handlers:
            print(f'{get_msg_count()}: EditBehavior: _do_selected: {self}: self.handlers is not empty!')
        else:
            for handler_type in self.handler_types:
                if handler_type in self.side_handlers:
                    edit_handler = EditHandler(handler_type=handler_type)
                if handler_type in self.corner_handlers:
                    edit_handler = CornerEditHandler(handler_type=handler_type)
                self.add_widget(edit_handler)
                edit_handler.update_pos(None, None)
                self.bind(pos=edit_handler.update_pos)
                self.bind(size=edit_handler.update_pos)
                self.handlers.append(edit_handler)

    def _do_deselected(self):
        self.selected = False
        if self.handlers:
            for handler in self.handlers:
                self.remove_widget(handler)
        else:
            print(f'{get_msg_count()}: EditBehavior: _do_deselected: {self}: self.handlers is empty!')
        self.handlers.clear()

    def on_touch_down(self, touch):
        self.current_size = self.size[0], self.size[1]
        self.current_pos = self.pos[0], self.pos[1]
        x, y = touch.pos
        collide_handler = False
        print(f'\t {get_msg_count()}: EditBehavior: on_touch_down: {self}')
        for handler in self.handlers:
            if handler.collide_point(x, y):
                collide_handler = True
                handler.bind_pos_to_parent()
        if collide_handler:
            pass
        elif self.collide_point(x, y):
            if self.selected:
                self._unbind_boxes()
            else:
                self._do_selected()
        else:
            self._do_deselected()
        return super(EditBehavior, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        self._unbind_boxes()
        print(f'\t {get_msg_count()}: EditBehavior: on_touch_up: {self}')
        return super(EditBehavior, self).on_touch_up(touch)

    def _unbind_boxes(self):
        for handler in self.handlers:
            handler.unbind_pos_to_parent()


if __name__ == '__main__':
    from kivy.uix.floatlayout import FloatLayout
    from kivy.lang import Builder
    from kivy.base import runTouchApp


    class EditableRectangle(EditBehavior, DragBehavior, Widget):
        pass


    Builder.load_string('''
#:set corner_handler_radius 8
#:set axis_handler_size 10
#:set corner_edit_overdraw_width 2 # 2.5
#:set handlers_color (1, 13/255, 0, 1)
#:set default_color (0, 111/255, 1, 1)

<CornerEditHandler>:
    drag_rectangle: self.x, self.y, self.width, self.height
    drag_timeout: 10000000
    drag_distance: 0
    size_hint: None, None
    size: corner_handler_radius, corner_handler_radius
    pos: 300, 300

    canvas:
        Color:
            rgba: handlers_color
        Ellipse:
            pos: self.pos
            size: self.size
        Color:
            rgba: rgba('#ff0000')            
        SmoothLine:
            overdraw_width:sp(corner_edit_overdraw_width)
            width:sp(1)
            circle: self.x+corner_handler_radius/2,self.y+corner_handler_radius/2,corner_handler_radius/2,0,360,200            
        
<EditHandler>:
    drag_rectangle: self.x, self.y, self.width, self.height
    drag_timeout: 10000000
    drag_distance: 0
    size_hint: None, None
    size: axis_handler_size, axis_handler_size
    pos: 300, 300

    canvas:
        Color:
            rgba: handlers_color
        Rectangle:
            pos: self.pos
            size: self.size    
    
<EditableRectangle>:
    id: my_rectangle
    drag_rectangle: self.x-1, self.y-1, self.width+1, self.height+1
    drag_timeout: 10000000
    drag_distance: 0
    size_hint: None, None
    size: 200, 100
    pos: self.parent.center_x, self.parent.center_y
    canvas:
        Color:
            rgba: default_color
        Line:
            width: 2.
            rectangle: (self.x, self.y, self.width, self.height)
    ''')
    fl = FloatLayout()
    fl.add_widget(EditableRectangle())
    runTouchApp(fl)
