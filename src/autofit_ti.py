'''
Code taken from https://gist.github.com/tshirtman/bd62828b98746dcecb1ed269b04459b9
Original author: https://gist.github.com/tshirtman
'''
from kivy.clock import mainthread
from kivy.core.text import Text
from kivy.uix.textinput import TextInput


class AdjustableSizeTextInput(TextInput):

    def update_font_size(self, widget):
        text = widget.text
        max_width = widget.width - (widget.border[1] + widget.border[3])
        max_height = widget.height - (widget.border[0] + widget.border[2])

        if not text:
            return
        instance = Text(text_size=(max_width, None), font_size=10, text=text)
        width, height = instance.render()

        while height < max_height:
            instance.options['font_size'] *= 2
            width, height = instance.render()

        while height > max_height:
            instance.options['font_size'] *= .95
            width, height = instance.render()

        widget.font_size = instance.options['font_size']
        self.reset_scroll(widget)

    @mainthread
    def reset_scroll(self, widget):
        widget.scroll_x = widget.scroll_y = 0
