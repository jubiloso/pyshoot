from datetime import datetime
from functools import partial

from kivy.app import App
from kivy.config import Config
from kivy.uix.behaviors import DragBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.widget import Widget

from src.EditBehavior import EditBehavior
from src.autofit_ti import AdjustableSizeTextInput


class EditableRectangle(EditBehavior, DragBehavior, Widget):
    pass


class EditableLabel(EditBehavior, DragBehavior, Label):
    pass


class EditableAdjustableText(EditBehavior, DragBehavior, Widget):
    pass


def current_timestamp():
    return datetime.now().strftime("%Y%m%d-%H%M%S-%f")


class RootWidget(BoxLayout):
    # Image by Garik Barseghyan from Pixabay
    default_img_src = '../resources/img/abstract-1264071_1280.png'
    default_label_txt = 'This is what I want to show you'
    pass

    def add_screenshot(self):
        img = Image(source=self.default_img_src)
        img.size = img.texture_size
        img.size_hint = (None, None)
        self.ids.main_image = img
        self.ids.float_layout.add_widget(img)

    def save(self):
        self.ids.float_layout.export_to_png(f'export-{current_timestamp()}.png')

    def add_label(self):
        label = EditableLabel()
        label.text = self.default_label_txt
        label.pos = self.center_x - label.size[0] / 2, self.center_y - label.size[1] / 2
        self.ids.float_layout.add_widget(label)

    def add_rectangle(self):
        rectangle = EditableRectangle()
        rectangle.pos = self.center_x - rectangle.size[0] / 2, self.center_y - rectangle.size[1] / 2
        self.ids.float_layout.add_widget(rectangle)

    def add_adjustable_text(self):
        wrapper_widget = EditableAdjustableText()
        adj_size_text = AdjustableSizeTextInput()
        wrapper_widget.add_widget(adj_size_text)
        wrapper_widget.bind(pos=partial(self.update_adj_text_pos, adj_size_text))
        wrapper_widget.bind(size=partial(self.update_adj_text_size, adj_size_text))
        wrapper_widget.pos = 500, 500
        wrapper_widget.size = 500, 150
        self.ids.float_layout.add_widget(wrapper_widget)

    def update_adj_text_pos(self, adj_text, rectangle, observable):
        adj_text.pos = rectangle.pos

    def update_adj_text_size(self, adj_text, rectangle, observable):
        adj_text.size = rectangle.size

    def clear_all(self):
        self.ids.float_layout.clear_widgets()
        self.add_screenshot()


class PyShootApp(App):
    def build(self):
        Config.set('graphics', 'width', '1400')
        Config.set('graphics', 'height', '1000')
        return RootWidget()


if __name__ == '__main__':
    PyShootApp().run()
